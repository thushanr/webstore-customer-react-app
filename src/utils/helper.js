
export const validateBid = (bidAmount) => {
    return Number.isInteger(bidAmount);
}

export const getDateTimeStr = (datetime) => {
    return window.moment(datetime).format('YYYY-MM-DD HH:mm:ss');
}



