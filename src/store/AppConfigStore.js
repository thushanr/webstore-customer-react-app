import {createStore, applyMiddleware, compose} from 'redux';
import rootReducer from '../reducers';
import thunk from 'redux-thunk';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


const AppConfigStore = (initialState) => {
    return createStore(
        rootReducer,
        initialState,
        composeEnhancers(
            applyMiddleware(thunk),
        )
        // compose(
        //     applyMiddleware(thunk),
        //     window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
        // )
    );
}

export default AppConfigStore;