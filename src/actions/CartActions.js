import * as types from './ActionTypes';
import CartApi from '../api/Cart';
import {inertServerErrorToStore} from './common/ErrorActions';

//add cart action
export const addToCart = (item, qty) => {

    return {
        type: types.ADD_TO_CART,
        item, qty
    }
}
//remove item action
export const removeItem = (id) => {
    return {
        type: types.REMOVE_ITEM,
        id
    }
}

export const changeQty = (id, qty) => {
    return {
        type: types.CHANGE_QTY,
        id, qty
    }
}

export const checkout = (cart) => {

    let data = prepareCheckoutData(cart);

    return (dispatch) => {
        CartApi.checkout(data).then(response => {
            let data = response.data;

            if (data.success === 1) {
                dispatch(checkoutSuccess(data));
            } else {
                dispatch(checkoutError(data));
            }
        }).catch(err => {
            console.error(err);
            dispatch(inertServerErrorToStore({msg: "An error occurred while your are trying proceeed checkout."}));
        });
    }

    function prepareCheckoutData(cart) {
        let data = {
            total: parseFloat(cart.total),
            cartItems: []
        }

        cart.items.map((item, key) => {
            let price = parseFloat(item.cartQty * item.price);
            data.cartItems.push({productId: item.id, qty: item.cartQty, price});
        });

        return data;
    }
}

export const checkoutSuccess = (cart) => {
    return {
        type: types.CHECKOUT_SUCCESS, ...cart
    }
}

export const checkoutError = (cart) => {
    return {
        type: types.CHECKOUT_ERROR, ...cart
    }
}

