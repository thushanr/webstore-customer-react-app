import * as types from '../ActionTypes';

export const  inertServerErrorToStore = (error) => {
    return {type: types.HTTP_ERROR, error};
}