import * as types from './ActionTypes';
import ProductApi from '../api/Product';
import {inertServerErrorToStore} from './common/ErrorActions';

export const list = (data) => {
    return (dispatch) => {
        return ProductApi.list(data).then(response => {
            if (response.data) {
                let productList = response.data;
                dispatch(success(productList));
            } else {

            }
        }).catch(err => {
            dispatch(inertServerErrorToStore(err));
        });
    };

    function success(productList) {
        return {type: types.GET_PRODUCT_LIST, productList}
    }
}

// export const signup = (data) => {
//     return (dispatch) => {
//         return UserApi.signup(data).then(response => {
//             if (response.data && response.data.success) {
//                 dispatch(success(true));
//             } else {
//                 dispatch(inertServerErrorToStore(response.data.message));
//             }
//         }).catch(err => {
//             dispatch(inertServerErrorToStore(err));
//         });
//     };

//     function success(signupSuccess) {
//         return {type: types.SIGNUP_SUCCESS, signupSuccess}
//     }
// }

