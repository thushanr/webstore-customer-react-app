import * as types from './ActionTypes';
import storage from '../utils/storage';
import UserApi from '../api/User';
import {inertServerErrorToStore} from './common/ErrorActions';

export const signup = (data) => {
    return (dispatch) => {
        return UserApi.signup(data).then(response => {
            if (response.data && response.data.success) {
                dispatch(success(true));
            } else {
                dispatch(inertServerErrorToStore(response.data.message));
            }
        }).catch(err => {
            dispatch(inertServerErrorToStore(err));
        });
    };

    function success(signupSuccess) {
        return {type: types.SIGNUP_SUCCESS, signupSuccess}
    }
}

export const login = (data) => {

    return (dispatch) => {

        return UserApi.login(data).then(response => {
            const isLoggedIn =  response.data.authToken ?  true :  false;

            if (isLoggedIn) {
                dispatch({type: types.LOGIN, isLoggedIn});

                //save in session storage
                storage.setData('authToken', response.data.authToken);
                storage.setData('userData', JSON.stringify(response.data.user));

            } else {
                dispatch(inertServerErrorToStore({msg: "Invalid email or password."}));
            }
        }).catch(err => {
            dispatch(inertServerErrorToStore({msg: 'An error occurred'}));
        });
    }
}

export const checkAuthenticated = (isLoggedIn) => {
    return (dispatch) => {
        dispatch({type: types.CHECK_AUTH, ...isLoggedIn});
    }
}

export const logout = () => {

    storage.removeUserAuthData();

    return (dispatch) => {
        dispatch({type: types.LOGOUT, isLoggedIn: false});
    }

    //TODO: uncomment below code once API ready.
    // return UserApi.logout().then(response => {
    //     storage.removeUserAuthData();
    // }).catch(err => {

    // });
}