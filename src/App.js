import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'

import moment from 'moment';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import './App.scss';
import './assets/styles/styles.css'
import './fontawesome';

import Layout from './components/layout/Layout';
import LayoutRoute from './components/layout/LayoutRoute';

import {checkAuthenticated} from './actions/UserActions';
import storage from './utils/storage';

import ProductPage from './components/pages/ProductsPage';
import SignupPage from './components/pages/SignupPage';
import * as AccountSettingsPage from './components/pages/AccountSettingsPage';
import * as SidebarContent from './components/layout/sidebar/content';
import CartPage from './components/pages/CartPage';
import OrderListPage from './components/pages/orders/OrderListPage';
import OrderDetailPage from './components/pages/orders/OrderDetailPage';

window.moment = moment;

class App extends React.Component {

	constructor(props) {
		super(props);

		const authToken = storage.getAuthToken();
		const isLoggedIn = authToken ? true : false;
		this.props.actions.checkAuthenticated({isLoggedIn});
	}

	render() {
		return (
			<div className="App">
				<Router>
					<Switch>
						<LayoutRoute
							exact
							pageTitle="Login or Create an Account"
							path="/signup"
							layout={Layout}
							noSidebar = {true}
							component = {props => (
								<SignupPage {...props}/>
							)}
						/>
						<LayoutRoute
							exact
							pageTitle="Login or Create an Account"
							path="/login"
							layout={Layout}
							noSidebar = {true}
							component = {props => (
								<SignupPage {...props}/>
							)}
						/>
						<LayoutRoute
							exact
							pageTitle="Products"
							path="/"
							layout = {Layout}
							component = {props => (
								<ProductPage {...props}/>
							)}
							sidebar = {props => (<SidebarContent.ProductsSidebar {...props}/>)}
						/>
						<LayoutRoute
							exact
							key="p"
							pageTitle="Update Account Settings"
							path="/account-settings"
							requireAuth
							layout = {Layout}
							component = {props => (
								<AccountSettingsPage.Dashboard {...props}/>
							)}
							sidebar = {props => (<SidebarContent.ManageSettingsSidebar {...props}/>)}
						/>
						<LayoutRoute
							exact
							pageTitle="Update Account Settings"
							path="/account-settings/business-informtion"
							requireAuth
							layout = {Layout}
							component = {props => (
								<AccountSettingsPage.BusinessInformation {...props}/>
							)}
							sidebar = {props => (<SidebarContent.ManageSettingsSidebar {...props}/>)}
						/>
						<LayoutRoute
							exact
							pageTitle="Update Account Settings"
							path="/account-settings/authorized-contact"
							requireAuth
							layout = {Layout}
							component = {props => (
								<AccountSettingsPage.AuthorizedContact {...props}/>
							)}
							sidebar = {props => (<SidebarContent.ManageSettingsSidebar {...props}/>)}
						/>
						<LayoutRoute
							exact
							pageTitle="Shopping Cart"
							path="/my-cart"
							requireAuth
							layout = {Layout}
							component = {props => (
								<CartPage {...props}/>
							)}
							noSidebar = {true}
						/>
						<LayoutRoute
							exact
							pageTitle="My Orders"
							path="/my-orders"
							requireAuth
							layout = {Layout}
							component = {props => (
								<OrderListPage {...props}/>
							)}
							sidebar = {props => (<SidebarContent.ManageSettingsSidebar {...props}/>)}
						/>
						<LayoutRoute
							exact
							pageTitle="Order Detail"
							path="/my-order/:orderId"
							requireAuth
							layout = {Layout}
							component = {props => (
								<OrderDetailPage {...props}/>
							)}
							sidebar = {props => (<SidebarContent.ManageSettingsSidebar {...props}/>)}
						/>
					</Switch>
				</Router>
			</div>
		);
	}
}

const mapDispathToProps = dispatch => {
    return {
        actions: {
            checkAuthenticated: bindActionCreators(checkAuthenticated, dispatch),
        }
    }
}
export default connect(null, mapDispathToProps)(App);
