import { library } from '@fortawesome/fontawesome-svg-core';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {
    faCartPlus, faTrashAlt, faArrowRight, faPhoneAlt, faCamera
} from '@fortawesome/free-solid-svg-icons'

library.add(
    faArrowRight,
    faCartPlus,
    faPhoneAlt,
    faTrashAlt,
    faCamera,
);