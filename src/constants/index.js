export const API_BASE_URL = process.env.REACT_APP_API_BASE_URL;
export const API_ENDPOINT_PREFIX = '/frontend/v1';

export const STORAGE_PROVIDER = 'LOCAL' //other values - SESSION, COOKIE