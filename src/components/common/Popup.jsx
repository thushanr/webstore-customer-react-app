import React, { useState } from 'react';
import {connect} from 'react-redux';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {NotificationManager} from 'react-notifications';

const Popup = (props) => {
    const {
        opener,
        className,
        title,
        body,
        size,
        popup
    } = props;

    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);

    const isPromise = (p) => {
        return p && Object.prototype.toString.call(p) === "[object Promise]";
    }

    const primaryAction = () => {
        let p = popup.primaryBtn.action();

        isPromise(p) && p.then(result => {
            if (!result.error) {
                NotificationManager.success(result.message || "Successfully saved.");
                toggle();
            }
        }).catch(err => {
            console.error(err.response || err);
            NotificationManager.error(err.response.data.message || 'An error occured.')
        });
    }

    return (
        <>
            <span onClick={toggle}>{opener}</span>
            <div>
                <Modal size={size || 'lg'}  isOpen={modal} toggle={toggle} className={className}>
                    <ModalHeader toggle={toggle}>{title}</ModalHeader>
                    <ModalBody>
                        {body}
                    </ModalBody>
                    <ModalFooter>
                        {popup.primaryBtnText !== '' && (<Button color="primary" onClick={primaryAction}>{popup.primaryBtnText}</Button>)}{' '}
                        <Button color="secondary" onClick={toggle}>{popup.secondaryBtnText}</Button>
                    </ModalFooter>
                </Modal>
            </div>
        </>
    )
}

const mapStateToProps = state => {
    return {
        popup: state.popup
    }
}

export default connect(mapStateToProps, null)(Popup);