import React, { Children } from 'react';
import {Label, Input} from 'reactstrap';
import './Common.scss';

const InlineSelect = props => (
    <div className={props.containerClass + " inline-field"}>
        <Label check>{props.label}</Label>
        <div className="inline-field__controls">
            <Input type="select" name={props.name}>
                {props.options && props.options.map((option) => (
                    <option key={option.value} value={option.value}>{option.label}</option>
                ))}
            </Input>
        </div>
    </div>
)

export default InlineSelect;