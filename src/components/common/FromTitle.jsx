import React from 'react';
import './Common.scss';

const FormTitle = ({children}) => (
    <strong className='form-title'>{children}</strong>
);

export default FormTitle;