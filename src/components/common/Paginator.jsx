import React from 'react';
import Pagination from 'react-bootstrap/Pagination';
// import PageItem from 'react-bootstrap/PageItem';

const Paginator = () => (
    <Pagination>
        <Pagination.First />
        <Pagination.Prev />
        <Pagination.Item>{1}</Pagination.Item>
        <Pagination.Ellipsis />

        <Pagination.Item>{11}</Pagination.Item>
        <Pagination.Item active>{12}</Pagination.Item>

        <Pagination.Ellipsis />
        <Pagination.Item>{20}</Pagination.Item>
        <Pagination.Next />
        <Pagination.Last />
    </Pagination>
);

export default Paginator;