import React from 'react';
import './Page.scss';

const PageHeader = ({children}) => (
    <div className="page-heading container-fluid">
        <div className="page-title">
            <h2>{children}</h2>
        </div>
    </div>
);

export default PageHeader;