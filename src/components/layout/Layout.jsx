import React, { Children } from 'react';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom'

import Header from './header/Header';
import Sidebar from './sidebar/Sidebar';
import Page from './page/Page';
import PageHeader from './page/PageHeader';

import {NotificationContainer} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import './Layout.scss';

const Layout = (props) => {

    const pageClass = props.noSidebar ? 'col-main col-sm-12 col-sm-push-3' : 'col-main col-sm-9 col-sm-push-3';

    // Redirect if not logged in.
    if  (props.requireAuth && !props.user.isLoggedIn) {
        return (<Redirect to="/login"></Redirect>)
    }

    return (
        <div>
            <Header/>
            <PageHeader>{props.pageTitle}</PageHeader>
            <section className="main-container col2-left-layout bounceInUp animated">
                <div className="container">
                    <div className="row">

                        {!props.noSidebar && (
                            <Sidebar isLoggedIn={props.user.isLoggedIn}>{props.sidebar}</Sidebar>
                        )}
                        <div className={pageClass}>
                            <Page>
                                {props.component}
                            </Page>
                        </div>
                    </div>
                </div>
            </section>

            <NotificationContainer/>

        </div>
    )
};

const mapStateToProps = state => {
    return {
        user: state.UserReducer
    }
}

export default connect(mapStateToProps, null)(Layout);

// export default Layout;