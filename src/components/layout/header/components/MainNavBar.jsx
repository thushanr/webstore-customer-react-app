import React, {useState} from 'react';
import {connect} from 'react-redux';
import {Redirect} from "react-router-dom";
import logo from '../../../../assets/logo/logo.svg';
import storage from '../../../../utils/storage';
import { Navbar,Nav,NavDropdown} from 'react-bootstrap'
import {logout} from '../../../../actions/UserActions'
import MiniCart from './MiniCart';

const MainNavBar = (props) => {

    const [redirect, setRedirect] = useState('');

    const userData = JSON.parse(storage.getUserData()) || {};

    const handleLogout = () => {

        console.log("props", props);
        props.actions.logout();
        setRedirect("/login");
    }

    return (
        <>
            {redirect && <Redirect to={redirect}/>}
            <div className="container">
                <div className="row">
                    <div id="mainNavBar">
                        <div className = "main-nav-container">
                            <div className="header-logo">
                                <a className="logo">
                                    <img height="50" src={logo} alt="Divolgo Sports" className="brand-image img-circle elevation-3" style={{opacity: '.8'}} />
                                </a>
                            </div>

                            <div className="nav-bar">
                                <Navbar bg="light" expand="lg">
                                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                                    <Navbar.Collapse id="basic-navbar-nav">
                                        <Nav className="mr-auto">
                                            <Nav.Link href="/">Products</Nav.Link>
                                            <Nav.Link href="#link">About Us</Nav.Link>

                                            {!userData.id && <Nav.Link href="/signup">Sign In or Register</Nav.Link>}

                                            {userData.id && (
                                                <NavDropdown title={"Hi " + userData.companyName} id="basic-nav-dropdown">
                                                    <NavDropdown.Item href="/account-settings">My Account</NavDropdown.Item>
                                                    <NavDropdown.Item href="/my-orders">My Orders</NavDropdown.Item>
                                                    <NavDropdown.Divider />
                                                    <NavDropdown.Item onClick={handleLogout}>Sign Out</NavDropdown.Item>
                                                </NavDropdown>
                                            )}
                                        </Nav>

                                        <MiniCart/>

                                    </Navbar.Collapse>
                                </Navbar>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
};

const mapDispathToProps = dispatch => {
    return {
        actions: {
            logout: () => { dispatch(logout()) },
        }
    }
}

export default connect(null, mapDispathToProps)(MainNavBar);