import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {Button, Input, Row, Col} from 'reactstrap'
import {Redirect } from "react-router-dom";

import {connect} from 'react-redux';
import { removeItem, addToCart } from '../../../../actions/CartActions';

import './MiniCart.scss';

const logo = 'https://via.placeholder.com/150';

class MiniCart extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            redirect: null,
            isCartOpen: false,
            isCartEmpty: true,
            items:[] ,
        }
    }

    componentDidMount() {
        let cartItems = sessionStorage.getItem("cart");
        if (cartItems) {
            let cartJson = JSON.parse(cartItems);
            cartJson.count > 0 && cartJson.items.map((item, key) => {
                this.props.addToCart(item, item.cartQty);
            });
        }
    }

    handleClickCartIcon = () => {
        let isCartEmpty = this.props.cart.count > 0 ? false : true;
        !isCartEmpty && this.setState({isCartOpen: !this.state.isCartOpen,  isCartEmpty});
    }

    handleClickRemove = (id) => {
        this.props.removeItem(id)
        let isCartEmpty = this.props.cart.count > 0 ? false : true;
        isCartEmpty && this.setState({isCartOpen: false, isCartEmpty});
    }

    hanldeClickCheckout = (e) => {
        this.setState({redirect: '/my-cart/', isCartOpen: false});
        console.log(this.state.redirect);
    }

    render() {

        const cartItems = this.props.cart.items || [];
        const isCartEmpty = this.props.cart.count > 0 ? false : true;
        const itemCountText = !isCartEmpty && (this.props.cart.count + ' ' + ( this.props.cart.count > 1 ? 'items' : 'item'));

        return (
            <div className="mini-cart">

                <div className="basket">
                    <a onClick={this.handleClickCartIcon} onBlur={this.handleBlurCartIcon}>
                        <FontAwesomeIcon icon="cart-plus"/>
                        {!isCartEmpty && <span>{this.props.cart.count}</span> }
                    </a>
                </div>

                <div className={`fl-mini-cart-content ${!isCartEmpty && this.state.isCartOpen && 'show'} `}>
                    <div className="block-subtitle">
                        <div className="top-subtotal">{itemCountText}, <span className="price">${this.props.cart.total}</span> </div>
                    </div>

                    <ul className="mini-products-list" id="cart-sidebar">

                        {cartItems.map((item, key) => {
                            return(
                                <li className="item first" key={key}>
                                    <div className="item-inner">
                                        <a className="product-image" title={item.name} href="#l">
                                            <img  width="100%" src={logo} alt="Card image cap" />
                                        </a>
                                        <div className="product-details">
                                            <Row>
                                                <Col xs="7">
                                                    <span className="price">${item.price}</span>
                                                    <p className="product-name"><a href="accessories-detail.html">{item.name}</a></p>
                                                </Col>
                                                <Col xs="3" className="qty">
                                                    {item.cartQty}
                                                </Col>
                                                <Col xs="2" className="item-action">
                                                    <span className="text-dark cursor-pointer" onClick={() => this.handleClickRemove(item.id)}>
                                                        <FontAwesomeIcon icon="trash-alt"/>
                                                    </span>
                                                </Col>
                                            </Row>
                                        </div>
                                    </div>
                                </li>
                            )
                        })}
                    </ul>
                    <div className="actions">
                        <Button className="btn-checkout" title="Checkout" type="button" color="primary" onClick={this.hanldeClickCheckout}>
                            <span>Checkout</span>
                        </Button>
                    </div>
                </div>

                {this.state.redirect && <Redirect to={this.state.redirect}/>}

            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      cart: state.cart,
    }
}

const mapDispatchToProps= (dispatch) => {
    return {
        removeItem: (productId) => { dispatch(removeItem(productId)) },
        addToCart: (product) => { dispatch(addToCart(product)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MiniCart);
