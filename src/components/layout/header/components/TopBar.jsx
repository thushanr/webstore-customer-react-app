import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'


const TopBar = () => (
    <div className="container">
        <div className="row">
            <div className="col-lg-6 col-xs-12 col-sm-6 col-md-6">
                <div className="offers">

                    <i> <FontAwesomeIcon/>10% off</i>
                </div>
            </div>
            <div className="col-lg-6 col-lg-6 col-xs-12 col-sm-6 col-md-6 call-us">
                <i>
                    <FontAwesomeIcon icon="phone-alt"/>{" "}
                    +1 800 789 0000
                </i>
            </div>
        </div>
    </div>
)

export default TopBar;