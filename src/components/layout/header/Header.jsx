import React from 'react';
import TopBar from './components/TopBar';
import MainNavBar from './components/MainNavBar';
import './Header.scss';

import PropTypes from 'prop-types'

const Header = ({children}) => (
    <header className="header">
        <div className="header-banner">
            <TopBar></TopBar>
        </div>

        <MainNavBar/>

    </header>
);

export default Header;