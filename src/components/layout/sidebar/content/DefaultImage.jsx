import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import Modal from '../../../common/Popup';
import ManageImage from '../../../pages/logo/MangeBrandImage';

import './DefaultImage.scss';

const DefaultImage = () => {

    const btnUpdate = (
        <div id="text">
            <FontAwesomeIcon icon="camera"/>
            <span>Update</span>
        </div>
    )

    return (
        <>
            <div className="section-filter">
                <div className="default-image-wrap">
                    <div className="img-block">
                        <img width="180" src="https://storage.googleapis.com/controller-data-public/brand-images/62_410617.jpg"/>
                        <div id="overlay">
                            <Modal
                                size = "xl"
                                title = "Manage Images"
                                opener = {btnUpdate}
                                body={<ManageImage/>}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default DefaultImage;
