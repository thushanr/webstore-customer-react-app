import React, {useState, useEffect} from 'react';
import {Row, Col, Label, Input, FormGroup} from 'reactstrap';
import ProductFiltersApi from '../../../../api/ProductFilter';

const ProductssSidebar = () => {

    const [tournaments, setTournaments] = useState([]);
    const [matches, setMatches] = useState([]);
    const [tournament, setTournament] = useState(0);
    const [match, setMatch] = useState(0);

    const handleChangeTournament = (e) => {
        setTournament(e.target.value);
    }

    const handleChangeMatch = (e) => {
        setMatch(e.target.value);
    }

    useEffect(() => {
        ProductFiltersApi.tournaments().then(response => {
            setTournaments(response.data.tournaments || []);
        }).catch(err => {
            console.error(err);
        });
    },[])

    useEffect(() => {
        ProductFiltersApi.matches(tournament).then(response => {
            setMatches(response.data.matches || []);
        }).catch(err => {
            console.error(err);
        });
    }, [tournament])

    return (
        <>
            <h2>Search Products</h2>

            <Row>
                <Col xs="12">
                    <FormGroup>
                        <Label>Tournament</Label>
                        <Input type="select" value={tournament} onChange={handleChangeTournament}>
                            <option value="0">ALL</option>
                            {tournaments.map((tournament, key) => {
                                return <option key={key} value={tournament.id}>{tournament.name}</option>
                            })}
                        </Input>
                    </FormGroup>
                </Col>

                <Col xs="12">
                    <FormGroup>
                        <Label>Match</Label>
                        <Input type="select" value={match} onChange={handleChangeMatch}>
                            <option value="0">--</option>
                            {matches.map((match, key) => {
                                return <option key={key} value={match.id}>{match.name}</option>
                            })}
                        </Input>
                    </FormGroup>
                </Col>

                <Col xs="12">
                    <FormGroup>
                        <Label>Type</Label>
                        <Label>
                            <Input type="radio" name="productType"/>Buy Now
                        </Label>
                        <Label>
                            <Input type="radio"  name="productType" />Auction
                        </Label>
                    </FormGroup>
                </Col>

                <Col xs="12">
                    <FormGroup>
                        <Label>
                            <Input type="checkbox" name="productType"/>FEATURED
                        </Label>
                        <Label>
                            <Input type="checkbox"  name="productType" />ENDING SOON
                        </Label>
                    </FormGroup>
                </Col>
            </Row>
        </>
    )
};

export default ProductssSidebar;