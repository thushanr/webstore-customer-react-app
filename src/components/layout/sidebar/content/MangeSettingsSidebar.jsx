import React from 'react';
import { Link } from 'react-router-dom';
import {Row, Col, Label, Input, FormGroup, ListGroup, ListGroupItem} from 'reactstrap';


const ManageSettingsSidebar = () => {

    return (
        <>
            <h2>Account Settings</h2>
            <Row>
                <Col xs="12">
                    <ListGroup>
                        <ListGroupItem action>
                            <Link to="/account-settings">Account Dashboard</Link>
                        </ListGroupItem>
                        <ListGroupItem action>
                            <Link to="/account-settings/business-informtion">Business Information</Link>
                        </ListGroupItem>
                        <ListGroupItem action>
                            <Link to="/account-settings/authorized-contact">Authorized Contact Information</Link>
                        </ListGroupItem>
                        <ListGroupItem action>
                            <Link to="">Payment Method</Link>
                        </ListGroupItem>
                    </ListGroup>
                </Col>
            </Row>
        </>
    )
};

export default ManageSettingsSidebar;