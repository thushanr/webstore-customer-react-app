import React from 'react';
import {Link} from 'react-router-dom'
import DefaultImage from './content/DefaultImage';
import './Sidebar.scss';

const Sidebar = (props) => {

    console.log("propsprops",props);
    return(
        <aside className="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9 wow bounceInUp animated animated">

            {props.isLoggedIn && <DefaultImage/>}

            <div className="section-filter">
                <div className="b-filter__inner bg-grey">
                    {props.children}
                </div>
            </div>
        </aside>
    )
}

export default Sidebar;