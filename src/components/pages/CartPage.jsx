import React from 'react';

import {connect} from 'react-redux';
import { removeItem, changeQty, checkout} from '../../actions/CartActions';
import {Link} from 'react-router-dom';

import {Button} from 'reactstrap';
import {Redirect} from "react-router-dom";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

import CartItem from './cart/CartItem';
import './CartPage.scss';

class CartPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            redirect: null,
        };
    }

    handleChangeQty = (itemId, qty) => {
        if (qty < 1) {
            this.props.removeItem(itemId);
        } else {
            this.props.changeQty(itemId, qty);
        }
    }

    handleRemoveItem = (itemId) => {
        this.props.removeItem(itemId);
    }

    handleClickContinue = () => {
        this.setState({redirect: '/'})
    }

    handleClickProceedCheckout = (event) => {
        this.props.checkout(this.props.cart);
    }

    render() {

        const items = this.props.cart.items || [];
        const isCartEmpty = this.props.cart.count > 0 ? false : true;
        const isCheckoutSuccess = this.props.cart.checkout && this.props.cart.checkout.success === 1 ? true : false;

        return (
            <>
                { this.state.redirect && <Redirect to={this.state.redirect}/>}

                {!isCheckoutSuccess && isCartEmpty && <p className="text-center">Cart is empty.</p>}

                {isCartEmpty && isCheckoutSuccess &&
                    <div class="alert alert-success checkout-success" role="alert">
                        <h4 class="alert-heading">Thank you for your order.</h4>
                        <hr/>
                        <p>
                            Your order has been placed successfully with Divolgo. Your order number is:
                            <span className="font-weight-bold">{this.props.cart.checkout.orderIdFormatted}</span> <br/>
                            Click here to <Link className="btn btn-primary" to={"/my-order/" + this.props.cart.checkout.orderId}>View Your Order</Link><br/>
                        </p>
                    </div>
                }

                {!isCartEmpty &&
                    <div className="main-cart-container">
                        <table className="main-cart-table table table-hover">
                            <thead className="thead-light">
                                <tr>
                                    <th scope="col" width="10%"></th>
                                    <th scope="col" width="55%">PRODUCT</th>
                                    <th scope="col" width="10%" className="text-right">UNIT PRICE</th>
                                    <th scope="col" width="10%" className="text-center">QTY</th>
                                    <th scope="col" width="10%" className="text-right">SUBTOTAL</th>
                                    <th scope="col" width="5%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                {items.map((item, key) => {
                                    return <CartItem
                                        key = {key}
                                        handleChangeQty = {this.handleChangeQty}
                                        handleRemoveItem = {this.handleRemoveItem}
                                        {...item}
                                    />
                                })}
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colSpan="4"  className="text-right">
                                        <h4>Grand Total</h4>
                                    </td>
                                    <td colSpan="2" className="text-right">
                                        <h4>${this.props.cart.total}</h4>
                                    </td>
                                </tr>
                                <tr className="controls-row">
                                    <td colSpan="3">
                                        <Button onClick={this.handleClickContinue}>CONTINUE SHOPPING {" "}
                                            <FontAwesomeIcon icon="arrow-right"/>
                                        </Button>
                                    </td>
                                    <td colSpan="3">
                                        <Button className="float-right" color="primary" onClick={this.handleClickProceedCheckout}>PROCEED TO CHECKOUT</Button>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                }
            </>
        )
    }
}

const mapStateToProps = (state) => {
    return {
      cart: state.cart,
    }
}

const mapDispatchToProps= (dispatch) => {
    return {
        removeItem: (itemId) => { dispatch(removeItem(itemId)) },
        changeQty: (itemId, qty) => {dispatch(changeQty(itemId, qty))},
        checkout: (cart) => {dispatch(checkout(cart))},
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CartPage);