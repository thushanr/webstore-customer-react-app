import React from 'react';
import {Link} from 'react-router-dom'

import OrderApi from '../../../api/Order';

class OrderListPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            orders: []
        };
    }

    componentDidMount() {
        OrderApi.list().then(response => {
            let orders = response.data.orders || [];
            this.setState({orders: orders});
        }).catch(err => {
            this.setState({order: []});
            console.warn(err);
        });
    }

    render() {

        return(
            <div className="order-list-container">
                <table className="main-cart-table table table-hover">
                    <thead className="thead-light">
                        <tr>
                            <th scope="col" width="35%">ORDER #</th>
                            <th scope="col" width="20%" className="">DATE</th>
                            <th scope="col" width="10%">STATUS</th>
                            <th scope="col" width="15%" className="text-right">ORDER TOTAL</th>
                            <th scope="col" width="15%"></th>
                        </tr>
                    </thead>

                    <tbody>
                        {this.state.orders.map((order, key) => {
                            return <tr key={key}>
                                <td>{order.orderNumber}</td>
                                <td>{order.createdAtFormatted}</td>
                                <td>{order.statusLabel}</td>
                                <td className="text-right">${order.totalAmount}</td>
                                <td  className="text-right">
                                    <Link to={`/my-order/${order.id}`}>View</Link>
                                </td>
                            </tr>
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default OrderListPage;