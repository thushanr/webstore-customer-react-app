import React from 'react';
import {Link} from 'react-router-dom'
import BlockTitle from '../../common/FromTitle';


import OrderApi from '../../../api/Order';

import './OrderDetailPage.scss';

class OrderDetailPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            order: {}
        }
    }

    componentDidMount() {
        const orderId = this.props.match.params.orderId;

        OrderApi.get(orderId).then(response => {
            let data = response.data;
            this.setState({order: data});
        }).catch(err => {
            console.error(err);
        });
    }

    render() {

        const orderItems = this.state.order.orderItems || [];

        return (
            <div className="order-details-container">
                <h1>Order # {this.state.order.orderNumber} <span className="order-status">{this.state.order.statusLabel}</span> </h1>
                <span>{this.state.order.createdAt}</span>

                <table class="table table-borderless order-items-table">
                    <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th className="text-right" scope="col">Price</th>
                            <th className="text-right" scope="col">Qty</th>
                            <th className="text-right" scope="col">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orderItems.map((item, key) => {
                            return <tr key={key}>
                                <td>[Product Name]</td>
                                <td className="text-right">{item.price}</td>
                                <td className="text-right">{item.qtyOrdered}</td>
                                <td className="text-right">{item.qtyOrdered * item.price}</td>
                            </tr>
                        })};
                    </tbody>
                    <tfoot>
                        <tr>
                            <th className="text-right" colSpan="3">Grand Total</th>
                            <th className="text-right">${this.state.order.totalAmount}</th>
                        </tr>
                    </tfoot>
                </table>

                <div id="order-other-info">
                    <BlockTitle>Order Information</BlockTitle>
                    <table class="table table-borderless">
                        <thead>
                            <th>Billing Address</th>
                            <th>Payment Method</th>
                        </thead>
                    </table>


                </div>
            </div>
        );
    }

}

export default OrderDetailPage;