import React, {useState, useEffect} from 'react';
import {Row, Col} from "reactstrap";
import BrandImageItem from './BrandImageCard';

const UploadedImages = (props) => {

    const {images} = props;

    const [defaultImage, setDefaultImage] =  useState();

    const handleChange = (e) => {
        setDefaultImage(e.target.value);
    }

    return (
        <>
            <Row>
                {images && images.map((image, key) => {
                    return <BrandImageItem
                        key = {key}
                        isDefault = {true}
                        logo = {image.brandImageItems}
                        handleChange = {handleChange}
                        value = {image.id}
                        defaultImage = {defaultImage}
                        flag = {props.flag}
                    />
                })}

                {images.length <= 0 && <Col><br/><br/>No Items</Col>}
            </Row>
        </>
    )
}

export default UploadedImages;