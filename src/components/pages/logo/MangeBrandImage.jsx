import React, { useState, useEffect } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Card, Button, CardTitle, CardText, Row, Col } from 'reactstrap';
import classnames from 'classnames';

import {connect} from 'react-redux';
import {setPopupMetaData} from '../../../actions/common/PopupActions';

import BrandImageApi from '../../../api/BrandImage';

import UploadeImage from './UploadImage';
import UploadedImages from './UploadedImages';
import './ManageImage.scss';

const MangeBrandImage = (props) => {

    const [activeTab, setActiveTab] = useState('1');
    const [savedImages, setSavedImages] = useState([]);

    const tabPanes = [
        {tabId: '1', title: 'Approved Images'},
        {tabId: '2', title: 'Pending Approval'},
        {tabId: '3', title: 'Upload New Image'},
        {tabId: '4', title: 'Rejected Images'},
    ]

    useEffect(() => {
        setPopupMetaData(activeTab);
    }, []);

    const toggle = tab => {
        setPopupMetaData(tab);
        if (activeTab !== tab) setActiveTab(tab);
    }

    const getApprovedImages= () => {
        BrandImageApi.getApprovedImages().then(response => {
            setSavedImages(response.data || []);
        }).catch(err => {
            console.error(err.response || err);
        })
    }

    const getPendingImages= () => {
        BrandImageApi.getPendingImages().then(response => {
            setSavedImages(response.data || []);
        }).catch(err => {
            console.error(err.response || err);
        })
    }

    const getRejectedImages = () => {
        BrandImageApi.getRejectedImages().then(response => {
            setSavedImages(response.data || []);
        }).catch(err => {
            console.error(err.response || err);
        })
    }

    const setPopupMetaData = (tab) => {
        if (tab  == 1) { // Approved Images
            getApprovedImages();
            props.setPopupMetaData({primaryBtnText: 'Set as default image', primaryBtn: {action: () => {}}});

        } else if (tab == 2) { // Pending Images]
            getPendingImages();
            props.setPopupMetaData({primaryBtnText: '', primaryBtn: {action: () => {}} });

        } else if (tab == 3) { //Upload new Image
            props.setPopupMetaData({primaryBtnText: 'Upload', primaryBtn: {action: () => {}}});

        } else if (tab == 4) { //Rejected Image
            getRejectedImages();
            props.setPopupMetaData({primaryBtnText: '', primaryBtn: {action: () => {}}});
        }
    }

    const renderTabContent = (props) => {

        console.log(props);

        return(
            <NavItem key={props.tabId}>
                <NavLink
                    className={classnames({ active: activeTab === props.tabId })} onClick={() => {toggle(props.tabId);}}>
                    {props.title}
                </NavLink>
            </NavItem>
        );
    }

    return (
        <div>
            <Nav tabs>
                {
                    tabPanes.map((tabPane, key) => {
                        return renderTabContent({...tabPane})
                    })
                }
            </Nav>

            <TabContent activeTab={activeTab}>
                <TabPane tabId="1">
                    <UploadedImages images={savedImages} flag={'approved'}/>
                </TabPane>

                <TabPane tabId="3" id="upload-new-image-tab">
                    <UploadeImage/>
                </TabPane>

                <TabPane tabId="2">
                    <UploadedImages images={savedImages} flag={'pending'}/>
                </TabPane>

                <TabPane tabId="4">
                    <UploadedImages images={savedImages} flag={'reject'}/>
                </TabPane>
            </TabContent>
        </div>
    );
}
const mapDispatchToProps = (dispatch) => {
    return {
        setPopupMetaData: (metadata) => { dispatch(setPopupMetaData(metadata)) }
    }
}

export default connect(null, mapDispatchToProps)(MangeBrandImage);
