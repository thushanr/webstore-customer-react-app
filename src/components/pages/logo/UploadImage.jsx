import React from 'react';
import ImageUploader from 'react-images-upload';
import ImageCropper from './ImageCropper';
import BrandImageApi from '../../../api/BrandImage';
import {API_BASE_URL} from '../../../constants';

class UploadImage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            imageUri: '',
            imagePath: ''
        };
        this.onDrop = this.onDrop.bind(this);
    }

    onDrop(images) {

        const brandImage = images.length > 0 ? images[0]: {};
        const imageData = {brandImage};

        BrandImageApi.upload(imageData).then(response => {
            const imagePath = response.data.imagePath || '';
            const imageUri = API_BASE_URL + imagePath;
            this.setState({imageUri, imagePath});
        }).catch(err => {
            console.error(err);
        });
    }

    render() {
        return (
            <>
                <ImageUploader
                    withIcon = {true}
                    buttonText = 'Choose images'
                    onChange = {this.onDrop}
                    imgExtension = {['.jpg', '.jpeg', '.png']}
                    maxFileSize = {5242880}
                    singleImage = {true}
                />

                {this.state.imageUri && (<ImageCropper imageUri = {this.state.imageUri} imagePath={this.state.imagePath}/>)}

            </>
        )
    }
}

export default UploadImage;