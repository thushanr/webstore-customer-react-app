
import React, {useState, useEffect} from 'react';
import Cropper from 'react-easy-crop';
import Slider from '@material-ui/core/Slider';

import {connect} from 'react-redux';
import {primaryAction, } from '../../../actions/common/PopupActions';
import BrandImageApi from '../../../api/BrandImage';


const ImageCropper = (props) => {

    const {imageUri, imagePath} = props;
    const [crop, onCropChange] = useState({ x: 0, y: 0 });
    const [zoom, onZoomChange] = useState(1);
    const [croppedAreaPixels, setcroppedAreaPixels] = useState({});

    const aspect = 4 / 3,
        cropSize = {width: 400, height: 240};

    const onCropComplete = (area, croppedAreaPixels) => {
        setcroppedAreaPixels(croppedAreaPixels);
    }

    const onSave = () => {
        let p = new Promise(function (resolve, reject) {
            BrandImageApi.crop({imagePath, ...croppedAreaPixels}).then(response => {
                const res = response.data;
                if (res && res.success === 1) {
                    resolve({error: false, message: "New image has been saved successfully."});
                } else {
                    reject(res.message);
                }
            }).catch(err => {
                console.error(err);
                reject(err);
            });
        });
       return p;
    }

    useEffect(() => {
        props.primaryAction({primaryBtn: {action: () => onSave() }});
    }, [croppedAreaPixels]);

    return (
        <>
            <div className="crop-container">
                <Cropper
                    image = {imageUri}
                    crop = {crop}
                    zoom = {zoom}
                    zoomWithScroll = {true}
                    aspect = {aspect}
                    cropSize = {cropSize}
                    restrictPosition = {false}
                    onCropChange = {onCropChange}
                    onCropComplete = {onCropComplete}
                    onZoomChange = {onZoomChange}
                />
            </div>

            <div className="controls">
                <Slider
                    value = {zoom}
                    min = {1}
                    max = {3}
                    step = {0.1}
                    aria-labelledby = "Zoom"
                    onChange = {(e, zoom) => onZoomChange(zoom)}
                />
            </div>
        </>
    );
}

// export default ImageCropper;

const mapDispatchToProps = (dispatch) => {
    return {
        primaryAction: (metadata) => { dispatch(primaryAction(metadata)) }
    }
}

export default connect(null, mapDispatchToProps)(ImageCropper);
