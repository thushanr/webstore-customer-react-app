import React, {useState, useEffect} from 'react';
import {Col, Card, CardImg, CardBody, CardTitle} from "reactstrap";
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';

const BrandImageCard = (props) => {

    const {logo} = props;
    const [src, setSrc] = useState("");

    useEffect(() => {
        logo && logo.map((img, k) => {
            if (img.imageType === 'BW') {
                setSrc(img.gcpUrl);
            }
        })
    }, [logo]);

    return (
        <Col xs="6" sm="4" md="3">
            <Card  color={props.defaultImage == props.value ? "info" : ''} >
                <CardImg top width="100%" src={src}/>
                <CardBody>
                    <CardTitle>
                        {
                            props.flag == 'approved' &&
                            <FormControlLabel value="female" control={<Radio  name="test" value={props.value} checked={props.defaultImage == props.value}  onChange = {props.handleChange}/>} label="Select as default" />
                        }
                    </CardTitle>
                </CardBody>
            </Card>
        </Col>
    )
}

export default BrandImageCard;