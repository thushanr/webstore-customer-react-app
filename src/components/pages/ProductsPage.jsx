import React from 'react';
import {Row} from "reactstrap";
import {connect} from 'react-redux';
import { addToCart } from '../../actions/CartActions';
import ProductCards from './productPage/ProductCard';
import Paginator from '../common/Paginator';
import InlineSelect from '../common/InlineSelect';

import ProductApi from '../../api/Product';
import './ProductsPage.scss';

const logo = 'https://via.placeholder.com/150';

class ProductsPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            products: []
        }
    }

    componentDidMount() {

        ProductApi.list().then(response => {
            this.setState({products: Array.isArray(response.data) ? response.data : []});
        }).catch(err => {
            console.error(err);
        })
    }

    handleClickPlaceBid = (auctionId, bidValue, bidMethod) => {
        ProductApi.placeBid({auctionId, bidValue, bidMethod});
    }

    handleClickAddToCart = (product) => {
        if (this.props.user.isLoggedIn) {
            this.props.addToCart(product);
        } else {
            this.props.history.push('/login');
        }
    }

    render() {

        let products = this.state.products;

        return (
            <div>
                <Row className="toolbar row">
                    <div className="col-md-6 col-xs-12">
                        <Paginator/>
                    </div>
                    <div className="col-md-3 col-xs-6 limiter">
                        <InlineSelect name="limiter" label="Show" options={[{label:10, value:10},{label:50, value:50},{label:100, value:100}]}/>
                    </div>
                    <div className="col-md-3 col-xs-6 sorter ">
                        <InlineSelect
                            name="sorter"
                            label="Sort By"
                            options={[
                                {label:'Position', value: 'position'},
                                {label:'Name', value: 'name'},
                                {label:'Price', value:'price'}]
                            }
                        />
                    </div>
                </Row>
                <Row>
                    { products.map((product, key) => {
                        return (
                            <>
                                <ProductCards
                                    key = {key}
                                    size = '3'
                                    className = 'no-right-padding'
                                    isLoggedIn = {this.props.user.isLoggedIn}
                                    title = {product.name}
                                    description = {product.description}
                                    handleClickPlaceBid = {this.handleClickPlaceBid}
                                    handleClickAddToCart = {this.handleClickAddToCart}
                                    product = {product}
                                />
                            </>
                        )
                    })}
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
      items: state.items,
      user: state.UserReducer
    }
}

const mapDispatchToProps= (dispatch)=>{

    return {
        addToCart: (product) => { dispatch(addToCart(product)) }
    }
}

// export default connect(mapStateToProps,mapDispatchToProps)(Home)


// const mapStateToProps = state => {
//     return {
//         error: state.ErrorReducer,
//         products: state.ProductReducer
//     }
// }

// const mapDispathToProps = dispatch => {
//     return {
//         actions: {
//             list: bindActionCreators(list, dispatch),
//         }
//     }
// }

// export default connect(mapStateToProps, mapDispathToProps)(ProductsPage);
// const mapStateToProps = state => {
//     return {
//         user: state.UserReducer
//     }
// }

export default connect(mapStateToProps, mapDispatchToProps)(ProductsPage);
