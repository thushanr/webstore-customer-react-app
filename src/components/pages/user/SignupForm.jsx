import React from 'react';
import {Form, Button} from 'react-bootstrap';

//https://jasonwatmore.com/post/2017/12/07/react-redux-jwt-authentication-tutorial-example

const SignupForm = (props) => {

    
    props.validator.message('confirmPassword', props.data.confirmPassword, `required|in:${props.data.password}`);


    return (
        <Form>
            <Form.Group>
                <Form.Label>Brand</Form.Label>
                <Form.Control name="brandName" type="text" placeholder="Your brand name" onChange={props.handleChange}/>
                <Form.Text className="text-danger">
                    {props.validator.message('brandName', props.data.brandName, 'required')}
                </Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>Company</Form.Label>
                <Form.Control name="companyName" type="text" onChange={props.handleChange}/>
                <Form.Text className="text-danger">
                    {props.validator.message('companyName', props.data.companyName, 'required')}
                </Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>Email</Form.Label>
                <Form.Control name="email" type="email" onChange={props.handleChange}/>
                <Form.Text className="text-danger">
                    {props.validator.message('email', props.data.email, 'required')}
                </Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>Password</Form.Label>
                <Form.Control name="password" type="password" onChange={props.handleChange}/>
                <Form.Text className="text-danger">
                    {props.validator.message('password', props.data.password, 'required')}
                </Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>Confirm Password</Form.Label>
                <Form.Control name="confirmPassword" type="password" placeholder="Confirm Password" onChange={props.handleChange}/>
                <Form.Text className="text-danger">
                    {!props.validator.fieldValid('confirmPassword') && 'Password mismatched'}
                </Form.Text>
            </Form.Group>
            
            <Button variant="primary" type="button" onClick = {props.handleClick}>
                Signup
            </Button>
        </Form>
    );
}

export default SignupForm;