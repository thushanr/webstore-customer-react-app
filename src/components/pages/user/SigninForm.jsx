import React from 'react';
import {Form, Button} from 'react-bootstrap';
//import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import SimpleReactValidator from 'simple-react-validator';
import {login} from '../../../actions/UserActions'

const SigninForm = (props) => {

    return (
        <Form>
            <Form.Group  controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control name="username" type="email" placeholder="Enter email" onChange={props.handleChange}/>
                <Form.Text className="text-danger">
                    {props.validator.message('username', props.data.username, 'required')}
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control name="password" type="password" placeholder="Password" onChange={props.handleChange}/>
                <Form.Text className="text-danger">
                    {props.validator.message('password', props.data.password, 'required')}
                </Form.Text>
            </Form.Group>

            <Button variant="primary" type="button" onClick={props.handleClick}>
                Login
            </Button>
        </Form>
    )

}
export default SigninForm;