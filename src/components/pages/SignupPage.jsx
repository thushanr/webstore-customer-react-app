import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import SimpleReactValidator from 'simple-react-validator';

import FormTitle from '../common/FromTitle';
import SignupForm from './user/SignupForm.jsx';
import SigninForm from './user/SigninForm.jsx';
import './Common.scss';

import {login, signup} from '../../actions/UserActions'
import { FormLabel } from 'react-bootstrap';

class SignupPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            login: {

            },
            signup: {

            }
        }

        this.loginValidator = new SimpleReactValidator({
            autoForceUpdate: this,
        });

        this.signupValidator = new SimpleReactValidator({
            autoForceUpdate: this,
        });
    }

    componentDidUpdate() {
        const {history} = this.props;
        const isLoggedIn  = this.props.user.isLoggedIn;
        if (isLoggedIn) {
            history.push("/");
        }
        //Auto login after signup
        if (this.props.user.signupSuccess) {
            this.props.actions.login({username: this.state.signup.email, password: this.state.signup.password});
        }
    }

    handleClickLogin = event => {
        if (!this.loginValidator.allValid()) {
            this.loginValidator.showMessages();
            return;
        }
        this.props.actions.login(this.state.login);
    }

    handleClickSignup = event => {
        if (!this.signupValidator.allValid()) {
            this.signupValidator.showMessages();
            return;
        }
        this.props.actions.signup(this.state.signup)
    }

    handleChangeLogin = event => {
        this.setState({...this.state, login: {...this.state.login, [event.target.name]: event.target.value}});
    }

    handleChangeSignup = event => {
        this.setState({...this.state, signup: {...this.state.signup, [event.target.name]: event.target.value}});
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-5">
                    <FormTitle>Sign In, if you have an account</FormTitle>

                    {this.props.error && this.props.error.msg && (<FormLabel className="text-danger">{this.props.error.msg}</FormLabel>) }

                    <SigninForm data={this.state.login} validator = {this.loginValidator} handleClick = {this.handleClickLogin} handleChange={this.handleChangeLogin}/>
                </div>

                <div className="col-md-7">
                    <div className="col-md-10 float-right">
                        <FormTitle>New to Divolgo?</FormTitle>
                        <SignupForm data={this.state.signup} validator = {this.signupValidator} handleClick = {this.handleClickSignup} handleChange={this.handleChangeSignup}/>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        error: state.ErrorReducer,
        user: state.UserReducer
    }
}

const mapDispathToProps = dispatch => {
    return {
        actions: {
            login: bindActionCreators(login, dispatch),
            signup: bindActionCreators(signup, dispatch)
        }
    }
}

export default connect(mapStateToProps, mapDispathToProps)(SignupPage);

