import React from 'react';
import { Row, Col, Label } from "reactstrap";
import { Card, CardImg, CardText, CardBody, CardTitle, Button, Input} from 'reactstrap';
import Clock from './Clock';
import {validateBid, getDateTimeStr} from '../../../utils/helper';

import './ProductCard.scss';

const logo = 'https://via.placeholder.com/150';

class ProductCard extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            bidAmount: '',
            placeBidButtonText: 'Place a Bid',
            addToCartButtonText: 'Add to Cart',
            bidError: '',
        }
    }

    handleChange = (e) => {
        this.setState({bidError: ''});
        this.setState({bidAmount: e.target.value});
    };

    handleClickPlaceBid = () => {

        if (!validateBid(this.state.bidAmount)) {
            this.setState({bidError: 'Enter valid bid amount!'});
        } else {
           this.props.handleClickPlaceBid(this.props.product.id, this.state.bidAmount, 0);
        }
    }

    handleClickAddToCart = (e) => {

    }

    renderBidCount = () => {
        const bidCount = Number.parseInt(this.props.product.bidCount || 0);
        const bidLabel = bidCount + (bidCount > 1 ? ' bids' : ' bid');

        return (
            <span>({bidLabel})</span>
        )
    }

    renderNextBidAmount = () => {
        let nextBidValue = this.props.product.currentPrice + this.props.product.minBidIncrement;
        return <span>Enter USD {nextBidValue} or more</span>
    }

    render() {
        const isAuction = this.props.product.sellingMethod === 'AUCTION' ? true : false;
        const clockTitle = this.props.product.sellingMethod === 'AUCTION' ? 'Auction closes in :' : 'Product expires in :';
        const clickTime = getDateTimeStr(this.props.product.endAt);

        return (

            <Col md={this.props.size}  className = "no-right-padding">
                <Card className="product-card">
                    <CardImg top width="100%" src={logo} alt="Card image cap" />
                    <CardBody>
                        <CardTitle>{this.props.title}</CardTitle>
                        <CardText>{this.props.description}</CardText>
                        <CardText tag="div" >
                            <Clock title = {clockTitle} time={clickTime}/>
                        </CardText>

                        {isAuction && (
                            <CardText className="current-price-wrap" tag="div">
                                Current Price : {this.props.product.currentPrice} {this.renderBidCount()}
                            </CardText>
                        )}

                        <CardText tag="div" >
                            {
                                isAuction ? (
                                    <div className="place-bid-wrap">
                                        <Row className="grouped-components row">
                                            <Col xs="5"  className = "no-right-padding">
                                                <Input type="text" name="bidAmount" onChange={this.handleChange}/>
                                            </Col>
                                            <Col xs="7">
                                                <Button color="primary" className={`btn-bid ${!this.props.isLoggedIn ? 'not-loggedin' : ''}`}
                                                    onClick={() => this.handleClickPlaceBid()}/>
                                            </Col>
                                        </Row>

                                        <div className="text-danger">{this.state.bidError}</div>
                                        <div className="next-bid-value">{this.renderNextBidAmount()}</div>
                                        <div className="auto-bid">
                                            <Label check>
                                                <Input type="checkbox" id="autoBid"  size="xs"/>
                                                Place auto bid
                                            </Label>
                                        </div>
                                    </div>
                                ) :
                                <>
                                    <Row className="add-to-cart-wrap" >
                                        <Col xs="12">
                                            <Button
                                                className={`btn-block btn-addtocart ${!this.props.isLoggedIn ? 'not-loggedin' : ''}`}
                                                color="success"
                                                onClick = {() => this.props.handleClickAddToCart(this.props.product)}
                                            />
                                        </Col>
                                    </Row>
                                </>
                            }
                        </CardText>
                    </CardBody>
                </Card>
            </Col>
        );
    }
};



export default ProductCard;