import React from 'react';
import {Input,Button} from 'reactstrap';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

const logo = 'https://via.placeholder.com/150';

class CartItem extends React.Component {

    constructor(props) {
        super(props);
    }

    handleChangeQty = (event) => {
        let qty = event.target.value;
        this.props.handleChangeQty(this.props.id, qty)
    }

    handleRemoveItem = (event) => {
        this.props.handleRemoveItem(this.props.id);
    }

    render() {

        return(
            <>
                <tr className={this.props.error == 1 && 'error-item'}>
                    <th scope="row">
                        <img src={logo} height="40"/>
                    </th>
                    <td>
                        <span>{this.props.name}</span><br/>
                        {this.props.error && <span className="text-danger">{this.props.message}</span>}
                    </td>
                    <td className="text-right">{this.props.price}</td>
                    <td className="text-right">
                        <span className="qty">
                            <Input type="number" defaultValue = {this.props.cartQty} min={1} max={this.props.availableQty} onChange = {this.handleChangeQty}/>
                        </span>
                    </td>
                    <td className="text-right" >{(this.props.cartQty  * this.props.price)}</td>
                    <td>
                        <a size="xs" href="#" className="remove-item" onClick = {this.handleRemoveItem}>
                            <FontAwesomeIcon icon="trash-alt"/>
                        </a>

                    </td>
                </tr>
            </>
        );
    }
}

export default CartItem;