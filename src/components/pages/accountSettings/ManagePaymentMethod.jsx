import React, {useState, useEffect} from 'react';
import {Form ,FormGroup, FormText, Button, Input, Label} from 'reactstrap';
import FormTitle from '../../common/FromTitle';
import {NotificationManager} from 'react-notifications';


// import Typography from '@material-ui/core/Typography';
// import Box from '@material-ui/core/Box';

import AccountSettingsApi from '../../../api/AccountSettings';



const ManagePaymentMethod = (props) => {

    const [paymentMethodId, setPaymentMethodId] = useState('');

    useEffect(() => {
        setPaymentMethodId(props.customer.paymentMethodId);

    },[props.customer.paymentMethodId]);

    const handleChange = (e) => {
        setPaymentMethodId(e.target.value);
    }

    const handleSubmit = () => {
        const postdata = {
            paymentMethodId,
        };
        AccountSettingsApi.updatePaymentMethod(postdata).then(res => {
            let response = res.data;
            response.success === 1 ? NotificationManager.success(response.message) :
                NotificationManager.error(response.message);

        }).catch(err => {
            console.log(err);
            NotificationManager.error("An error occured.")
        })
    }

    const renderPaymentMethod = () => {

        return (
            <>
                <Input type="select" name="paymentMethod" onChange={handleChange} value={paymentMethodId}>
                    <option defaultValue="0">--</option>
                    {props.paymentMethods && props.paymentMethods.map((pm, key) => {
                        return <option key={key} value={pm.id} >{pm.name}</option>
                    })}
                </Input>
            </>
        );
    }

    return (
        <Form>
            <FormGroup>
                <Label>Payment Method</Label>
                {renderPaymentMethod()}
                <FormText className="text-danger"></FormText>
            </FormGroup>

            <FormGroup>
                <Button color="primary" type="button" className="float-right" onClick={handleSubmit}>
                    Save
                </Button>
            </FormGroup>

            <FormGroup><br/></FormGroup>
        </Form>
    )
}

export default ManagePaymentMethod;