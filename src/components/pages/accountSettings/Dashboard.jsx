import React from 'react';
import {Form, Button} from 'react-bootstrap';
import FormTitle from '../../common/FromTitle';
import ManagePaymentMethod from './ManagePaymentMethod';

import AccountSettingsApi from '../../../api/AccountSettings';


class Dashboard extends React.Component {

    state = {
        customer: {},
        paymentMethods: [],
    }

    componentDidMount() {
        AccountSettingsApi.getCustomerData().then(response => {
            const customer = response.data || {};
            this.setState({customer});
        }).catch(err => {
            console.error(err);
        });

        AccountSettingsApi.paymentMethods().then(response => {
            const paymentMethods = response.data.paymentMethods || [];
            this.setState({paymentMethods});
        }).catch(err => {
            console.error(err);
        });
    }

    render() {
        return (
            <div>
                <div className="">
                    <FormTitle>Account Information</FormTitle>
                </div>

                <div className="">
                    <FormTitle>Payment Method</FormTitle>
                    <ManagePaymentMethod customer = {this.state.customer} paymentMethods={this.state.paymentMethods}/>
                </div>
            </div>
        )
    }
}

export default Dashboard;