import React from 'react';
import {Form, Button} from 'react-bootstrap';
import FormTitle from '../../common/FromTitle';


const AuthorizedContact = (props) => {

    return (
        <Form>
            <FormTitle>Authorized Contact Information</FormTitle>
            <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control as="select" name="brandName" type="select" onChange={props.handleChange}>
                    <option>Mr</option>
                    <option>Mrs</option>
                </Form.Control>
                <Form.Text className="text-danger">

                </Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>First Name</Form.Label>
                <Form.Control name="firstName" type="text" onChange={props.handleChange}/>
                <Form.Text className="text-danger">

                </Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>Last Name</Form.Label>
                <Form.Control name="lastName" type="text" onChange={props.handleChange}/>
                <Form.Text className="text-danger">

                </Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>Designation</Form.Label>
                <Form.Control name="designation" type="text" onChange={props.handleChange}/>
                <Form.Text className="text-danger">

                </Form.Text>
            </Form.Group>

            <Form.Group>
                <Form.Label>Contact No</Form.Label>
                <Form.Control name="designation" type="text" onChange={props.handleChange}/>
                <Form.Text className="text-danger">

                </Form.Text>
            </Form.Group>

        </Form>
    )

}

export default AuthorizedContact;