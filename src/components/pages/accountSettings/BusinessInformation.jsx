import React from 'react';
import {Input} from 'reactstrap';
import {Form, Button, Row, Col} from 'react-bootstrap';
import FormTitle from '../../common/FromTitle';
import AccountSettingsApi from '../../../api/AccountSettings';
import CommonApi from '../../../api/Common';


class BusinessInformation extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            formData: {
                companyName: '',
            },
            customerData: {},

            industries: [],
        }
    }

    componentDidMount() {
        this.loadCustomerData();


        CommonApi.industries().then(response => {
            const industries = response.data.industries || [];
            this.setState({industries});


            console.log("aaa", industries);
        })
    }

    loadCustomerData = () => {
        AccountSettingsApi.getCustomerData().then(response => {
            const data = response.data || {};

            if (data) {
                const formData = {
                    companyName: data.companyName,
                    brandName: data.brand.brandName,
                };
                this.setState({formData:{...this.state.formData, ...formData}})
            }

        }).catch(err => {
            console.error(err);
        });
    }

    handleChange = (event) => {
        this.setState({formData: {...this.state.formData, [event.target.name]: event.target.value}})
    }

    handleSubmit = (event) => {
        event.preventDefault();
        console.log(this.state.formData);
        AccountSettingsApi.updateBusinessInformation(this.state.formData)
    }

    render() {
        return (
            <Form onSubmit={this.handleSubmit} method="post">
                <FormTitle>Business Information</FormTitle>
                <Form.Group>
                    <Form.Label>Brand</Form.Label>
                    <Form.Control name="brandName" type="text" onChange={this.handleChange}  defaultValue={this.state.formData.brandName} />
                    <Form.Text className="text-danger">

                    </Form.Text>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Company Name</Form.Label>
                    <Form.Control name="companyName" type="text" onChange={this.handleChange} defaultValue={this.state.formData.companyName}/>
                    <Form.Text className="text-danger">

                    </Form.Text>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Industry</Form.Label>
                    <Form.Control as="select" >
                        {this.state.industries.map((industry, key) => {
                            return (
                                <option value={industry.id}>{industry.name}</option>
                            )
                        })}

                    </Form.Control>
                    <Form.Text className="text-danger">

                    </Form.Text>
                </Form.Group>

                <Form.Group className="">
                    <Form.Label>Billing Address</Form.Label>
                    <Form.Control name="addressLine1" type="text" placeholder="Address Line 1" onChange={this.handleChange}/><br/>
                    <Form.Control name="addressLine2" type="text" placeholder="Address Line 2" onChange={this.handleChange}/><br/>
                    <Form.Control name="city" type="text" placeholder="City" onChange={this.handleChange}/><br/>
                    <Form.Control name="state" type="text" placeholder="State/Province" onChange={this.handleChange}/>
                    <Form.Text className="text-danger"></Form.Text>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Postal Code</Form.Label>
                    <Form.Control name="postalCode" type="text" onChange={this.handleChange}/><br/>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Country</Form.Label>
                    <Form.Control as="select" name="country">
                        <option>Sri Lanka</option>
                        <option>India</option>
                    </Form.Control>
                    <Form.Text className="text-danger"></Form.Text>
                </Form.Group>

                <Form.Group>
                    <Form.Label>Telephone</Form.Label>
                    <Form.Control type="telephone" name="telephone" onChange={this.handleChange}/>
                    {/* <Row>
                        <Col sm="9">
                            <Form.Control type="text"/>
                        </Col>
                        <Col sm="3">
                            <Form.Control type="text" placeholder = "Ext"/>
                        </Col>
                    </Row> */}
                </Form.Group>

                <Form.Group>
                    <Button variant="primary" type="submit" className="float-right">
                        Save
                    </Button>
                </Form.Group>

                <Form.Group><br/></Form.Group>

            </Form>
        )
    }
};

export default BusinessInformation;