import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import App from './App';
import * as serviceWorker from './serviceWorker';
import AppConfigStore from './store/AppConfigStore';

// import {list} from './actions/ProductActions';


const store = AppConfigStore();

// store.dispatch(list());

store.subscribe(() => console.log(store.getState()));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
