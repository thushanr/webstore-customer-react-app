export default {
    user: {
        email: '',
        firstName: '',
        lasttName: '',
        isLoggedIn: false,
    },
    product: {
        sellingMethod: '',
        productId: ''
    },
    productList:[],

    cart: {
        items: [],
        total: 0,
        count: 0,
        checkout: {
            success: '',
            orderId: '',
            orderIdFormatted: '',
        }
    }
};