import initialState from './InitialState';
import { ADD_TO_CART,REMOVE_ITEM,SUB_QUANTITY,ADD_QUANTITY, CHANGE_QTY, CHECKOUT, CHECKOUT_SUCCESS, CHECKOUT_ERROR} from '../../actions/ActionTypes'

// https://medium.com/@ayabellazreg/make-a-simple-shopping-cart-app-using-react-redux-1-3-fefde93e80c7

const CartReducer = (state = initialState.cart, action) => {

    let newstate = {};

    const updateSession = (cart) => {
        sessionStorage.setItem('cart', JSON.stringify(cart));
    }

    switch(action.type) {

        case ADD_TO_CART:

            let inputItem = action.item;
            let qty = action.qty || 1;
            //check if the action id exists in the addedItems
            let exists = state.items.find(item => inputItem.id === item.id);

            if (exists) {
                inputItem.cartQty += qty
                newstate =  {
                    ...state,
                    total: state.total + (qty *inputItem.price),
                }
            } else {
                //inputItem.qty = qty;
                let newTotal = state.total + (qty * inputItem.price);
                let newCount = state.count + 1;
                inputItem.cartQty = qty;

                newstate =  {
                    ...state,
                    items: [...state.items, inputItem],
                    total : newTotal,
                    count: newCount,
                }
            }

            updateSession(newstate)

            return newstate;

        case REMOVE_ITEM:
            let itemId = action.id;
            let itemToRemove = state.items.find(item => itemId === item.id)
            let rest = state.items.filter(item => itemId !== item.id)

            let newTotal = state.total - (itemToRemove.price * itemToRemove.cartQty);
            let newCount = state.count - 1;

            if (newTotal < 0) {
                newTotal = 0;
            }

            if (newCount < 0) {
                newCount = 0;
            }

            newstate =  {
                ...state,
                items: rest,
                total: newTotal,
                count: newCount,
            }

            updateSession(newstate)

            return newstate;

        case CHANGE_QTY:

            let addedItem = state.items.find(item => action.id === item.id)
            let newQty = action.qty || 0;

            if (newQty < 0) {
                newQty = 0;
            }

            let total = state.total - (addedItem.price * addedItem.cartQty) + (addedItem.price * newQty);
            addedItem.cartQty = newQty;

            newstate =  {
                ...state,
                total: total
            }

            updateSession(newstate);
            return newstate;

        case CHECKOUT_SUCCESS:

            let initialCart = initialState.cart;

            //reset cart sesssion
            updateSession(initialCart);

            newstate =  {
                ...initialCart,
                checkout: {
                    success: action.success,
                    orderId: action.data.orderId,
                    orderIdFormatted: action.data.orderIdFormatted
                }
            }

            return newstate;

        case CHECKOUT_ERROR:

            let inputErroritems = action.data || [];
            let errorCartItems = [];

            inputErroritems.map((inputErrorItem, key) => {
                let errorCartItem =  state.items.find(cartItem => inputErrorItem.productId === cartItem.id);
                errorCartItem.error =  1;
                errorCartItem.message = inputErrorItem.message;

                errorCartItems.push(errorCartItem);
            });

            newstate =  {
                ...state,
            }

            return newstate;

        default:
            return state;
    }
}

export default CartReducer;
