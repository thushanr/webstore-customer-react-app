import {combineReducers} from 'redux';

import UserReducer from './customer/UserReducer';
import ProductReducer from './customer/ProductReducer';
import cart from './customer/CartReducer';

import ErrorReducer from './common/ErrorReducer';
import popup from './common/PopupReducer';


const RootReducer = combineReducers({
    UserReducer,
    ProductReducer,
    ErrorReducer,
    cart,
    popup,
});

export default RootReducer;