import axios from 'axios';
import {API_BASE_URL, API_ENDPOINT_PREFIX} from '../constants';
import httpHeaders from '../utils/httpHeaders';

const URL = API_BASE_URL + API_ENDPOINT_PREFIX;
class Product {

    list = (data) => {
        let url = URL + '/products';
        console.log(url);
        return axios.get(url, data);
    }

    placeBid = (data) => {
        let url = URL + '/bid';
        return axios.post(url, data, httpHeaders.authHeader());
    }
}

export default new Product();