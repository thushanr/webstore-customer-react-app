import axios from 'axios';
import {API_BASE_URL, API_ENDPOINT_PREFIX} from '../constants';

const URL = API_BASE_URL;

class Common {
    industries = () => {
        let url = URL + '/common/industries';
        return axios.get(url);
    }
}

export default new Common();