import axios from 'axios';
import {API_BASE_URL, API_ENDPOINT_PREFIX} from '../constants';

const URL = API_BASE_URL + API_ENDPOINT_PREFIX;

class User {

    signup = (data) => {
        let url = URL + '/customer/register';
        return axios.post(url, data);
    }

    login = (data) => {
        let url = API_BASE_URL + '/user/auth';
        return axios.post(url, data);
    }

    logout = () => {
        console.error("Logout API not implemented.");
    }
}

export default new User();