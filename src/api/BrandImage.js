import axios from 'axios';
import {API_BASE_URL, API_ENDPOINT_PREFIX} from '../constants';
import httpHeaders from '../utils/httpHeaders';

const URL = API_BASE_URL + API_ENDPOINT_PREFIX;

class BrandImage {

    upload = (data) => {
        let url = URL + '/brand-image/upload';
        let fd = new FormData();
        Object.entries(data).forEach(([key, value]) => {
            fd.append(key, value);
        });

        return axios.post(url, fd, httpHeaders.authHeader());
    }

    crop = (data) => {
        let url = URL + '/brand-image/crop';
        return axios.post(url, data, httpHeaders.authHeader());
    }

    getApprovedImages = () => {
        let url = URL + '/brand-images/approved';
        return axios.get(url, httpHeaders.authHeader())
    }

    getPendingImages = () => {
        let url = URL + '/brand-images/pending';
        return axios.get(url, httpHeaders.authHeader())
    }

    getRejectedImages = () => {
        let url = URL + '/brand-images/rejected';
        return axios.get(url, httpHeaders.authHeader())
    }
}

export default new BrandImage();