import axios from 'axios';
import {API_BASE_URL, API_ENDPOINT_PREFIX} from '../constants';
import httpHeaders from '../utils/httpHeaders';

const URL = API_BASE_URL + API_ENDPOINT_PREFIX;

class Order {
    list = () => {
        let url = URL + '/orders';
        return axios.get(url, httpHeaders.authHeader());
    }

    get = (orderId) => {
        let url = URL + '/orders/' + orderId;
        return axios.get(url, httpHeaders.authHeader());
    }
}

export default new Order();